unit uPanel;

{$mode objfpc}{$H+}

interface

uses
  Controls, Classes, SysUtils, StrUtils, ExtCtrls, LConvEncoding, WinSock2, uHttp,
  uSocket, uLogger, fpjson, Math;

const
  PANEL_ALIVE_TIME = 35;
  LIST_GROW_HALFSTEP = 4;
  MAX_PKT_LEN = 128;

type

  TPanel = class;
  TOnSend = procedure(Sender: TPanel) of object;

  { TTexts }

  TText = record
    Id: integer;
    Text: string;
  end;

  TTexts = class
  private
    FCount: integer;
    FText: array of TText;
    function GetText(idx: integer): string;
    procedure SetCount(AValue: integer);
  public
    constructor Create(const str: string);
    property Count: integer read FCount write SetCount;
    property Text[idx: integer]: string read GetText; default;
  end;

  { TPanel }

  TPanel = class
  private
    FId: integer;
    FIp: u_long;
    FOnSend: TOnSend;
    FText: string;
    FAlive: integer;
    procedure SetOnSend(AValue: TOnSend);
    procedure SetText(AValue: string);
  public
    constructor Create(AId: integer; AIp: u_long);
    destructor Destroy; override;
    property Text: string read FText write SetText;
    property OnSend: TOnSend read FOnSend write SetOnSend;
  end;

  { TPanelList }

  TPanelList = class(TWinControl)
  private
    FTimer: TTimer;
    FSocket: TUdpSocket;
    FHttpGet: THttpGet;
    FPanels: array of TPanel;
    FCount: integer;
    function GetDataUrl: string;
    function GetInterval: integer;
    procedure SetDataUrl(AValue: string);
    procedure SetInterval(AValue: integer);
  protected
    procedure OnTimer(Sender: TObject);
    procedure DoLog(const str: string);
    procedure UpdPanel(AId: integer; AIp: u_long);
    function FindId(AId: byte): integer;
    function FindIp(AIp: u_long): integer;
    procedure Add(id: byte; ip: u_long);
    procedure Del(id: byte);
    procedure DelIdx(idx: integer);
    procedure OnGet(const str: string);
    procedure OnSend(Sender: TPanel);
  public
    constructor Create(TheOwner: TComponent); override;
    destructor Destroy; override;
    procedure SendText(AId: byte; const str: string);
    procedure OnRecvFrom(Sender: TUdpSocket; const str: string; var addr: TSockAddr);
    property Interval: integer read GetInterval write SetInterval;
    property DataUrl: string read GetDataUrl write SetDataUrl;
  end;

implementation

{ TTexts }

function TTexts.GetText(idx: integer): string;
var
  i: integer;
begin
  for i := 0 to Count - 1 do
    if FText[i].Id = idx then begin
      Result := FText[i].Text;
      Exit;
    end;
  Result := '';
end;

procedure TTexts.SetCount(AValue: integer);
begin
  if FCount = AValue then
    Exit;
  FCount := AValue;
  SetLength(FText, AValue);
end;

constructor TTexts.Create(const str: string);
var
  jd: TJSONData;
  ja: TJSONArray;
  i: integer;
begin
  jd := GetJSON(str);

  try
    ja := jd.GetPath('tickets') as TJSONArray;
  except
    jd.Free;
    raise;
  end;

  Count := ja.Count;

  try
    for i := 0 to ja.Count - 1 do begin
      FText[i].Id := ja[i].GetPath('numWindow').AsInteger;
      FText[i].Text := ja[i].GetPath('noPass').AsString;
    end;
  except
    Count := 0;
    jd.Free;
    raise;
  end;

  jd.Free;
end;

{ TPanelList }

function TPanelList.FindId(AId: byte): integer;
var
  i: integer;
begin
  Result := -1;
  for i := 0 to FCount - 1 do
    if FPanels[i].FId = AId then begin
      Result := i;
      Break;
    end;
end;

function TPanelList.FindIp(AIp: u_long): integer;
var
  i: integer;
begin
  Result := -1;
  for i := 0 to FCount - 1 do
    if FPanels[i].FIp = AIp then begin
      Result := i;
      Break;
    end;
end;

procedure TPanelList.DoLog(const str: string);
begin
  AddLog(ClassName + ': ' + str);
end;

procedure TPanelList.SendText(AId: byte; const str: string);
var
  nd: integer;
begin
  nd := FindId(AId);
  if nd >= 0 then
    FPanels[nd].Text := str;
end;

procedure TPanelList.SetInterval(AValue: integer);
begin
  FHttpGet.Interval := AValue;
end;

function TPanelList.GetInterval: integer;
begin
  Result := FHttpGet.Interval;
end;

function TPanelList.GetDataUrl: string;
begin
  Result := FHttpGet.Url;
end;

procedure TPanelList.SetDataUrl(AValue: string);
begin
  FHttpGet.Url := AValue;
end;

procedure TPanelList.OnTimer(Sender: TObject);
var
  i: integer;
begin
  i := 0;
  while i < FCount do begin
    Dec(FPanels[i].FAlive);
    if FPanels[i].FAlive = 0 then
      DelIdx(i)
    else
      Inc(i);
  end;
  FHttpGet.Run := FCount > 0;
end;

procedure TPanelList.Add(id: byte; ip: u_long);
var
  n: integer;
begin
  n := Length(FPanels);
  if n <= FCount then
    SetLength(FPanels, n + LIST_GROW_HALFSTEP * 2);
  FPanels[FCount] := TPanel.Create(id, ip);
  FPanels[FCount].OnSend := @OnSend;
  FPanels[FCount].Text := '';
  Inc(FCount);
end;

procedure TPanelList.Del(id: byte);
var
  idx: integer;
begin
  idx := FindId(id);
  DelIdx(idx);
end;

procedure TPanelList.DelIdx(idx: integer);
var
  i: integer;
begin
  if idx >= 0 then begin
    FPanels[idx].Free;
    Dec(FCount);
    for i := idx to FCount - 1 do begin
      FPanels[i] := FPanels[i + 1];
    end;
    idx := Length(FPanels);
    if idx - FCount > LIST_GROW_HALFSTEP * 3 then
      SetLength(FPanels, idx - LIST_GROW_HALFSTEP * 2);
  end;
end;

procedure TPanelList.OnGet(const str: string);
var
  txt: TTexts;
  i: integer;
begin
  try
    txt := TTexts.Create(str);
  except
    on e: Exception do begin
      AddLog(e.ToString);
      for i := 0 to FCount - 1 do begin
        FPanels[i].Text := '';
      end;
    end;
  end;
  for i := 0 to FCount - 1 do begin
    FPanels[i].Text := txt[FPanels[i].FId];
  end;
  txt.Free;
end;

procedure TPanelList.OnSend(Sender: TPanel);
var
  addr: TSockAddr;
begin
  addr.sa_family := AF_INET;
  addr.sin_port := htons(22223);
  addr.sin_addr.S_addr := Sender.FIp;
  FillByte(addr.sin_zero, SizeOf(addr.sin_zero), 0);
  FSocket.SendTo(addr, 'AT+TT=3, "' + UTF8ToCP1251(Sender.Text) + '", 1, 0, 2, 2');
  AddLog(Format('Табло №%d: "%s"', [Sender.FId, Sender.Text]));
end;

procedure TPanelList.UpdPanel(AId: integer; AIp: u_long);
var
  nd, np: integer;
begin
  // Ищем табло по Ip
  np := FindIp(AIp);
  if np >= 0 then begin
    // если табло с AIp найден, то сбрасываем счетчик
    FPanels[np].FAlive := PANEL_ALIVE_TIME;
    if FPanels[np].FId <> AId then begin
      // Если Id не совпадает ищем и удаляем табло с таким Id
      nd := FindId(AId);
      if nd >= 0 then
        DelIdx(nd);
      // и меняем Id
      FPanels[np].FId := AId;
    end;
  end else begin
    // иначе ищем табло по Id
    nd := FindId(AId);
    if nd >= 0 then begin
      // если табло с AId найден, то сбрасываем счетчик
      FPanels[nd].FAlive := PANEL_ALIVE_TIME;
      // и меняем Ip
      FPanels[nd].FIp := AIp;
    end else begin
      // если табло не найдено, создаем новый
      Add(AId, AIp);
    end;
  end;
end;

constructor TPanelList.Create(TheOwner: TComponent);

begin
  inherited Create(TheOwner);
  Parent := TWinControl(TheOwner);

  FSocket := TUdpSocket.Create(Self);
  FSocket.OnRecvFrom := @OnRecvFrom;

  FHttpGet := THttpGet.Create;
  FHttpGet.OnGet := @OnGet;

  FTimer := TTimer.Create(Self);
  FTimer.OnTimer := @OnTimer;
  FTimer.Interval := 1000;
  FTimer.Enabled := True;
end;

destructor TPanelList.Destroy;
var
  i: integer;
begin
  FHttpGet.Free;
  for i := 0 to FCount - 1 do
    FPanels[i].Free;
  inherited Destroy;
end;

procedure TPanelList.OnRecvFrom(Sender: TUdpSocket; const str: string; var addr: TSockAddr);
var
  tmp: string;
  p: integer;
  len: integer;
begin
  tmp := str;
  // Разбор полученной строки
  if AnsiStartsText('+SFS:1', tmp) and AnsiEndsText('Ok', tmp) then begin
    // Если ответ на запрос Id - обновляем список табло
    p := Pos('=', tmp);
    if p > 0 then begin
      tmp := MidStr(tmp, p + 1, Length(tmp) - p - 2);
      p := StrToIntDef(tmp, -1);
      if p >= 0 then
        UpdPanel(p, addr.sin_addr.S_addr);
    end;
  end else begin
    // Если пакет пришел с неизвестного адреса
    if FindIp(addr.sin_addr.S_addr) = -1 then begin
      len := max(Length(str), MAX_PKT_LEN);
      AddLog('Новый адрес ' + inet_ntoa(addr.sin_addr) + '. Получен пакет: "' +
        LeftStr(str, len) + '"');
    end;
    // Иначе отправляем запрос Id
    addr.sin_port := htons(22223);
    tmp := 'AT+SFS=1';
    Sender.SendTo(addr, tmp);
  end;
end;

{ TPanel }

constructor TPanel.Create(AId: integer; AIp: u_long);
begin
  FId := AId;
  FIp := AIp;
  FText := ' ';
  FAlive := PANEL_ALIVE_TIME;
  AddLog(Format('Добавлено табло Id: %d, Ip: %s.', [FId, inet_ntoa(TInAddr(FIp))]));
end;

destructor TPanel.Destroy;
begin
  AddLog(Format('Tабло Id: %d, Ip: %s удалено.', [FId, inet_ntoa(TInAddr(FIp))]));
  inherited Destroy;
end;

procedure TPanel.SetText(AValue: string);
begin
  if FText = AValue then
    Exit;
  FText := AValue;
  if Assigned(OnSend) then
    OnSend(Self);
end;

procedure TPanel.SetOnSend(AValue: TOnSend);
begin
  if FOnSend = AValue then
    Exit;
  FOnSend := AValue;
end;

end.
