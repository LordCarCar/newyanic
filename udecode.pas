unit uDecode;

{$mode objfpc}{$H+}

interface

uses
  SysUtils;

function deCode(var str: string): boolean;

implementation

function deCode(var str: string): boolean;
var
  i: integer;
  d, n, n1: cardinal;
  res: string;
begin

  Result := False;

  if not TryStrToInt('0x' + LeftStr(str, 8), integer(d)) then
    Exit;

  Delete(str, 1, 8);
  n := d;
  d := (d and $000000FF) xor (d shr 24);
  SetLength(res, d);
  i := 0;

  while Length(str) > 0 do begin
    if not TryStrToInt('0x' + LeftStr(str, 8), integer(d)) then
      Exit;
    Delete(str, 1, 8);
    n1 := d;
    d := d xor n;
    n := n1;
    Move(d, res[i * 4 + 1], 4);
    Inc(i);
  end;

  Result := True;
  str := res;
end;

end.

