unit uParams;

{$mode objfpc}{$H+}

interface

uses
  SysUtils, Forms, Controls, Dialogs, StdCtrls, ExtCtrls, Spin, IniFiles, uLogger;

type

  { TParamsForm }

  TParamsForm = class(TForm)
    ButtonOk: TButton;
    ButtonCancel: TButton;
    LoggingCheck: TCheckBox;
    LogLenghtLabel: TLabel;
    IntervalLabel: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    LogLengthEdit: TSpinEdit;
    DataUrlEdit: TLabeledEdit;
    IntervalEdit: TSpinEdit;
    LogLength: TSpinEdit;
    procedure ButtonOkClick(Sender: TObject);
    procedure ButtonCancelClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
  private

  public

  end;

var
  ParamsForm: TParamsForm;

implementation

{$R *.lfm}

uses
  uMain;

{ TParamsForm }

procedure TParamsForm.ButtonOkClick(Sender: TObject);
var
  ini: TIniFile;
begin
  Logger.LogToFile := LoggingCheck.Checked;
  with MainForm do begin
    Interval := IntervalEdit.Value;
    LogLength := LogLengthEdit.Value;
  end;

  ini := TIniFile.Create(ChangeFileExt(ParamStr(0), '.ini'));
  ini.WriteInteger('Server', 'Interval', IntervalEdit.Value);
  ini.writeBool('Parameters', 'Logging', LoggingCheck.Checked);
  ini.WriteInteger('Parameters', 'LogLength', LogLengthEdit.Value);
  ini.Free;

  Close;
end;

procedure TParamsForm.ButtonCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TParamsForm.FormActivate(Sender: TObject);
begin
  with MainForm do begin
    DataUrlEdit.Text := DataUrl;
    IntervalEdit.Value := Interval;
    LogLengthEdit.Value := LogLength;
  end;
  LoggingCheck.Checked := Logger.LogToFile;
end;

procedure TParamsForm.FormDeactivate(Sender: TObject);
begin
  Close;
end;

end.

