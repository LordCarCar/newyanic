program YanicControl;

{$mode objfpc}{$H+}

uses {$IFDEF UNIX} {$IFDEF UseCThreads}
  cthreads, {$ENDIF} {$ENDIF}
  Windows,
  Interfaces,
  Forms,
  uMain,
  uParams,
  uAbout;

{$R *.res}

begin
  RequireDerivedFormResource := True;
  Application.Scaled := True;
  Application.Initialize;

  // Проверка уже запущенной копии
  Mutex := CreateMutex(nil, False, 'YanicControlMutex');
  if GetLastError = ERROR_ALREADY_EXISTS then begin
    Application.MessageBox('Программа уже запущена', 'Yanic panel control',
      MB_ICONERROR or MB_OK);
    Exit;
  end;

  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TParamsForm, ParamsForm);
  Application.CreateForm(TAboutForm, AboutForm);
  Application.OnEndSession := @MainForm.EndSession;
  Application.Run;
end.

