unit uSocket;

{$mode objfpc}{$H+}

interface

uses
  Windows, Controls, Classes, SysUtils, ExtCtrls, WinSock2, uLogger;

const
  SOCKET_EMPTY_TIMEOUT = 25;
  WM_SOCKET = WM_USER + 1;

type
  TUdpSocket = class;
  TOnRecvFrom = procedure(Socket: TUdpSocket; const str: string; var addr: TSockAddr) of object;

  { TUdpSocket }

  TUdpSocket = class(TWinControl)
  private
    FOnRecvFrom: TOnRecvFrom;
    FTimer: TTimer;
    FTimeOut: integer;
    FSocket: TSocket;
    procedure SetOnRecvFrom(AValue: TOnRecvFrom);
  protected
    procedure TimerTick(Sender: TObject);
    procedure ResetTimer;
    procedure ReSocket;
    procedure CloseSocket;
    procedure SocketMsg(var msg: TMessage); message WM_SOCKET;
  public
    constructor Create(TheOwner: TComponent); override;
    destructor Destroy; override;
    procedure SendTo(const addr: TSockAddr; const str: string);
    property OnRecvFrom: TOnRecvFrom read FOnRecvFrom write SetOnRecvFrom;
  end;

implementation

function MyTrim(const str: string): string;
var
  i: integer;
begin
  Result := str;
  i := 1;
  while i <= Length(Result) do
    if Result[i] <= #32 then
      Delete(Result, i, 1)
    else
      Inc(i);
end;

{ TUdpSocket }

procedure TUdpSocket.SetOnRecvFrom(AValue: TOnRecvFrom);
begin
  if FOnRecvFrom = AValue then
    Exit;
  FOnRecvFrom := AValue;
end;

procedure TUdpSocket.TimerTick(Sender: TObject);
begin
  Dec(FTimeOut);
  if FTimeOut <= 0 then begin
    ReSocket;
  end;
end;

procedure TUdpSocket.ResetTimer;
begin
  FTimeOut := SOCKET_EMPTY_TIMEOUT;
end;

procedure TUdpSocket.ReSocket;
var
  addr: TSockAddr;
  err: integer;
begin
  ResetTimer;

  CloseSocket;

  FSocket := socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  if FSocket = INVALID_SOCKET then begin
    AddError('Socket', WSAGetLastError);
    Exit;
  end;

  addr.sin_family := AF_INET;
  addr.sin_port := htons(22225);
  addr.sin_addr.S_addr := INADDR_ANY;
  FillByte(addr.sin_zero, SizeOf(addr.sin_zero), 0);
  err := bind(FSocket, addr, SizeOf(addr));
  if err = SOCKET_ERROR then begin
    AddError('Bind', WSAGetLastError);
    CloseSocket;
    Exit;
  end;

  err := WSAAsyncSelect(FSocket, Handle, WM_SOCKET, FD_READ + FD_CLOSE);
  if err = SOCKET_ERROR then begin
    AddError('Bind', WSAGetLastError);
    CloseSocket;
    Exit;
  end;

  AddLog('Сокет 0x' + IntToHex(FSocket, 8) + ' открыт.');
end;

procedure TUdpSocket.CloseSocket;
begin
  if FSocket <> INVALID_SOCKET then begin
    WinSock2.closesocket(FSocket);
    AddLog('Сокет 0x' + IntToHex(FSocket, 8) + ' закрыт.');
    FSocket := INVALID_SOCKET;
  end;
end;

procedure TUdpSocket.SocketMsg(var msg: TMessage);
var
  addr: TSockAddr;
  alen, len: integer;
  err: integer;
  buf: string;
begin
  if TSocket(msg.wParam) <> FSocket then begin
    AddLog('SocketMsg Неизвестный сокет 0x' + IntToHex(FSocket, 8) + '.');
    Exit;
  end;

  err := WSAGetSelectError(msg.lParam);
  if err <> 0 then begin
    AddError('SocketMsg', err);
    ReSocket;
    Exit;
  end;

  case WSAGetSelectEvent(msg.lParam) of
    FD_READ:
    begin
      SetLength(buf, 1024);
      alen := SizeOf(addr);
      addr.sa_family := AF_INET;
      len := recvfrom(FSocket, buf[1], 1024, 0, addr, alen);
      if len = SOCKET_ERROR then begin
        AddError('RecvFrom ' + inet_ntoa(addr.sin_addr), WSAGetLastError);
        Exit;
      end;
      SetLength(buf, len);

      buf := MyTrim(buf);
      //      AddLog('RecvFrom "' + buf + '" from ' + inet_ntoa(addr.sin_addr));

      if Assigned(FOnRecvFrom) then
        FOnRecvFrom(Self, buf, addr);

      ResetTimer;
    end;

    FD_CLOSE:
    begin
      AddLog('SocketMsg Сокет 0x' + IntToHex(FSocket, 8) + 'закрыт.');
    end;
  end;

end;

constructor TUdpSocket.Create(TheOwner: TComponent);
begin
  FSocket := INVALID_SOCKET;

  inherited Create(TheOwner);
  Parent := TWinControl(TheOwner);

  FTimer := TTimer.Create(Self);
  FTimer.OnTimer := @TimerTick;
  FTimer.Interval := 1000;
  FTimer.Enabled := True;
end;

destructor TUdpSocket.Destroy;
begin
  CloseSocket;
  inherited Destroy;
end;

procedure TUdpSocket.SendTo(const addr: TSockAddr; const str: string);
var
  len: integer;
begin
  len := WinSock2.sendto(FSocket, str[1], Length(str), 0, addr, SizeOf(addr));
  if len = SOCKET_ERROR then begin
    AddError('SendTo', WSAGetLastError);
    Exit;
  end;
  //  AddLog('SendTo ' + inet_ntoa(addr.sin_addr) + ' "' + str + '"');
end;

var
  wd: TWSAData;

initialization
  WSAStartup(WINSOCK_VERSION, wd);

finalization
  WSACleanup;

end.

