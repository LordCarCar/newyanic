unit uLogger;

{$mode objfpc}{$H+}

{$I-}

interface

uses
  Classes, SysUtils, LazUTF8;

type
  TOnLog = procedure(Sender: TObject; const str: string) of object;

  { TLogger }

  TLogger = class
  private
    FLogFile: TextFile;
    FOnLog: TOnLog;
    FError: boolean;
    FLogToFile: boolean;
    procedure SetLogToFile(AValue: boolean);
    procedure SetOnLog(AValue: TOnLog);
  public
    constructor Create;
    destructor Destroy; override;
    procedure WriteToFile(str: string);
    property LogToFile: boolean read FLogToFile write SetLogToFile;
    property OnLog: TOnLog read FOnLog write SetOnLog;
  end;

procedure AddLog(const str: string; force: boolean = False);
procedure AddError(const str: string; err: integer);

var
  Logger: TLogger;

implementation

procedure AddLog(const str: string; force: boolean);
var
  tmp: string;
begin
  tmp := Format('%s - %s', [DateTimeToStr(Now), str]);
  if UTF8RightStr(tmp, 1) <> '.' then
    tmp := tmp + '.';
  with Logger do begin
    if Assigned(OnLog) then
      OnLog(Logger, tmp);
    if LogToFile or force then
      WriteToFile(tmp);
  end;
end;

procedure AddError(const str: string; err: integer);
begin
  AddLog(Format('%s (%d) %s', [str, err, SysErrorMessage(err)]));
end;

{ TLogger }

procedure TLogger.SetOnLog(AValue: TOnLog);
begin
  if FOnLog = AValue then
    Exit;
  FOnLog := AValue;
end;

procedure TLogger.SetLogToFile(AValue: boolean);
begin
  if FLogToFile = AValue then
    Exit;
  FLogToFile := AValue;
  if AValue then
    AddLog('Включена запись логов в файл')
  else
    AddLog('Запись логов в файл выключена', True);
end;

constructor TLogger.Create;
var
  err: integer;
begin
  AssignFile(FLogFile, ChangeFileExt(ParamStr(0), '.log'));
  Append(FLogFile);
  err := IOResult;
  if err <> 0 then begin
    Rewrite(FLogFile);
    err := IOResult;
    if err = 0 then
      Append(FLogFile);
    err := IOResult;
  end;
end;

destructor TLogger.Destroy;
begin
  CloseFile(FLogFile);
  inherited Destroy;
end;

procedure TLogger.WriteToFile(str: string);
var
  err: integer;
begin
  if FError then
    Exit;
  WriteLn(FLogFile, str);
  err := IOResult;
  if err = 0 then begin
    Flush(FLogFile);
    err := IOResult;
  end;
  if err <> 0 then begin
    FError := True;
    AddLog(Format('Ошибка записи в файл (%d).', [err]));
  end;
end;

initialization
  Logger := TLogger.Create;

finalization
  Logger.Free;

end.

