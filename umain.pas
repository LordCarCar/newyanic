unit uMain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, Menus, ComCtrls, StdCtrls, ExtCtrls, IniFiles,
  uPanel, uParams, uAbout, uDecode, uLogger;

type

  { TMainForm }

  TMainForm = class(TForm)
    MainMenu: TMainMenu;
    Memo1: TMemo;
    MenuFile: TMenuItem;
    MenuExit: TMenuItem;
    MenuAbout: TMenuItem;
    MenuParams: TMenuItem;
    StatusBar1: TStatusBar;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure MenuAboutClick(Sender: TObject);
    procedure MenuExitClick(Sender: TObject);
    procedure MenuParamsClick(Sender: TObject);
  private
    FLogLength: integer;
    FPanels: TPanelList;
    function GetDataUrl: string;
    function GetInterval: integer;
    procedure SetDataUrl(AValue: string);
    procedure SetInterval(AValue: integer);
  public
    procedure OnLog(Sender: TObject; const str: string);
    procedure EndSession(Sender: TObject);
    property Interval: integer read GetInterval write SetInterval;
    property LogLength: integer read FLogLength write FLogLength;
    property DataUrl: string read GetDataUrl write SetDataUrl;
  end;

var
  MainForm: TMainForm;
  Mutex: THandle;

implementation

{$R *.lfm}

{ TMainForm }

procedure TMainForm.OnLog(Sender: TObject; const str: string);
begin
  while (Memo1.Lines.Count > 0) and (Memo1.Lines.Count > FLogLength - 1) do
    Memo1.Lines.Delete(0);
  Memo1.Lines.Add(str);
end;

procedure TMainForm.EndSession(Sender: TObject);
begin
  AddLog('Завершение сессии пользователя.', True);
  FormDestroy(Self);
end;

procedure TMainForm.FormCreate(Sender: TObject);
var
  ini: TIniFile;
  url: string;
begin
  Logger.OnLog := @OnLog;
  AddLog('Программа запущена.', True);


  ini := TIniFile.Create(ChangeFileExt(ParamStr(0), '.ini'));

  Top := ini.ReadInteger('Program', 'Top', Top);
  Height := ini.ReadInteger('Program', 'Height ', Height);
  Left := ini.ReadInteger('Program', 'Left', Left);
  Width := ini.ReadInteger('Program', 'Width', Width);
  LogLength := ini.ReadInteger('Parameters', 'LogLength', 100);
  Logger.LogToFile := ini.ReadBool('Parameters', 'Logging', False);

  FPanels := TPanelList.Create(Self);
  url := ini.ReadString('Server', 'Url', '');
  if deCode(url) then
    DataUrl := url;
  Interval := ini.ReadInteger('Server', 'Interval', 1);

  ini.Free;

end;

procedure TMainForm.FormDestroy(Sender: TObject);
var
  ini: TIniFile;
begin
  FPanels.Free;

  ini := TIniFile.Create(ChangeFileExt(ParamStr(0), '.ini'));
  ini.WriteInteger('Program', 'Top', Top);
  ini.WriteInteger('Program', 'Height', Height);
  ini.WriteInteger('Program', 'Left', Left);
  ini.WriteInteger('Program', 'Width', Width);
  ini.Free;

  AddLog('Программа завершена.', True);
end;

procedure TMainForm.MenuAboutClick(Sender: TObject);
begin
  AboutForm.Show;
end;

procedure TMainForm.MenuExitClick(Sender: TObject);
begin
  Close;
end;

procedure TMainForm.MenuParamsClick(Sender: TObject);
begin
  ParamsForm.Show;
end;

function TMainForm.GetInterval: integer;
begin
  Result := FPanels.Interval;
end;

function TMainForm.GetDataUrl: string;
begin
  Result := FPanels.DataUrl;
end;

procedure TMainForm.SetDataUrl(AValue: string);
begin
  FPanels.DataUrl := AValue;
end;

procedure TMainForm.SetInterval(AValue: integer);
begin
  FPanels.Interval := AValue;
end;

end.

