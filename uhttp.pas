unit uHttp;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fphttpclient, uLogger;

type

  THttpGet = class;
  TOnGet = procedure(const str: string) of object;

  { THttpGet }

  THttpGet = class(TThread)
  private
    FClient: TFPHTTPClient;
    FData: string;
    FInterval: integer;
    FOnGet: TOnGet;
    FRun: boolean;
    FUrl: string;
    procedure SetInterval(AValue: integer);
    procedure SetOnGet(AValue: TOnGet);
    procedure SetRun(AValue: boolean);
    procedure SetUrl(AValue: string);
  protected
    procedure Execute; override;
    procedure DoOnGet;
  public
    constructor Create;
    destructor Destroy; override;
    property OnGet: TOnGet read FOnGet write SetOnGet;
    property Interval: integer read FInterval write SetInterval;
    property Run: boolean read FRun write SetRun;
    property Url: string read FUrl write SetUrl;
  end;

implementation


{ THttpGet }

procedure THttpGet.SetOnGet(AValue: TOnGet);
begin
  if FOnGet = AValue then
    Exit;
  FOnGet := AValue;
end;

procedure THttpGet.SetRun(AValue: boolean);
begin
  if FRun = AValue then
    Exit;
  FRun := AValue;
  if AValue then
    Resume;
end;

procedure THttpGet.SetUrl(AValue: string);
begin
  if FUrl = AValue then
    Exit;
  FUrl := AValue;
end;

procedure THttpGet.SetInterval(AValue: integer);
begin
  if FInterval = AValue then
    Exit;
  FInterval := AValue;
  FClient.IOTimeout := 5000 * FInterval;
end;

procedure THttpGet.Execute;
var
  time: integer;
begin
  while not Terminated do begin
    if not Run then
      Suspend;

    if Url <> '' then begin
      try
        FData := FClient.Get(Url);
      except
        on e: Exception do begin
          AddLog(e.ToString);
          // Пауза 5 интервалов до новой попытки
          time := 0;
          while (time < 5 * Interval) and not Terminated do begin
            if Run then
              Sleep(1000);
            Inc(time);
          end;
          Continue;
        end;
      end;
      if Run then
        Synchronize(@DoOnGet);
    end;
    time := 0;
    while (time < Interval) and not Terminated do begin
      if Run then
        Sleep(1000);
      Inc(time);
    end;
  end;
end;

procedure THttpGet.DoOnGet;
begin
  if Assigned(OnGet) then
    OnGet(FData);
end;

constructor THttpGet.Create;
begin
  inherited Create(True);
  FClient := TFPHTTPClient.Create(nil);
  Interval := 1;
end;

destructor THttpGet.Destroy;
begin
  FClient.Free;
  inherited Destroy;
end;

end.

